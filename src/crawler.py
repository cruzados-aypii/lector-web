#importes para LinkParser
from html.parser import HTMLParser
from urllib.request import urlopen
from urllib import parse
import sys
from lxml import html
#importes para _DeHTMLParser
from traceback import print_exc
from re import sub
from sys import stderr
#importes para crawler y extractor
import configparser
import re
import arbolb
import indice_invertido
from datetime import datetime
from unidecode import unidecode
from time import sleep
from urllib.error import HTTPError
#importes para acceder a la configuracion
import os
from sys import path
path.append(os.path.abspath(os.path.dirname(__file__))[:-4])

configuracion = configparser.ConfigParser(delimiters = ('=', ';'))
configuracion.read(path[-1] + '\\cfg\\cfg.ini')

class LinkParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        if tag == 'a':
            for(key, value) in attrs:
                if key == 'href':
                    newURL = parse.urljoin(self.baseURL, value)
                    self.links.append(newURL)


    def fetch_page(self, url):
        self.links = []
        self.baseURL = url
        response = urlopen(url)
        contentType = response.getheader('Content-type')
        if 'text/html' in contentType:
            encoding = response.headers.get_param('charset')
            data = response.read()
            htmlString = data.decode(encoding)
            self.feed(htmlString)
            return htmlString, self.links
        else:
            return "", []


class _DeHTMLParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.__text = []

    def handle_data(self, data):
        text = data.strip()
        if len(text) > 0 and not (self.lasttag == 'script' or self.lasttag == 'style'):
            text = sub('[ \t\r\n]+', ' ', text)
            self.__text.append(text + ' ')

    def handle_starttag(self, tag, attrs):
        if tag == 'p':
            self.__text.append('\n\n')
        elif tag == 'br':
            self.__text.append('\n')

    def handle_startendtag(self, tag, attrs):
        if tag == 'br':
            self.__text.append('\n\n')

    def text(self):
        return ''.join(self.__text).strip()


def dehtml(text):
    try:
        parser = _DeHTMLParser()
        parser.feed(text)
        parser.close()
        return parser.text()
    except:
        print_exc(file=stderr)
        return text

def obtener_hora():
	hora = datetime.now().__str__().replace('-', '/')[:19]
	return '[' + hora[8:10] + hora[4:8] + hora[:4] + hora[10:] + ']'

class Crawler(object):
	def __init__(self):
		self.tmin = float(configuracion["CRAWLER"]["Tmin"])

	def crawl(self, extractor):
		parser = LinkParser()
		#Abro el archivo de bitacora en modo 'a' (append, o sea agregar al final)
		with open(path[-1] + "\\cfg\\"+configuracion["CRAWLER"]["Log"], "a") as file_bitacora:
			try:
				file_bitacora.write('Crawler iniciado ' + obtener_hora() + '\n')
				frontera = re.findall(r'[\w\-\./\\:]+', configuracion["CRAWLER"]["URLs"])

				for semilla in frontera:
					try:
						html_crudo, links_encontrados = parser.fetch_page(semilla)
						extractor.extraer_texto(html_crudo, semilla)
						visitados = [semilla]
						pos = 0
						dominio = re.match(r'(https?://[\w\-]+\.[\w\-]+\.?[\w\-]*\.?[\w\-]*)', semilla).group()
						if len(dominio) == 0:
							continue

						while pos < len(links_encontrados):
							link = links_encontrados[pos]
							pos += 1
							try:
								if not link.startswith(dominio) or link in visitados or re.search(r'(#[\w\-]*$|\.[\w\-]*$)', link) != None:
									continue	#Me aseguro que el link pertenezca al dominio y que no haya sido visitado o tan solo contenga un archivo
								sleep(self.tmin)
								visitados.append(link)
								html_crudo, temp_links = parser.fetch_page(link)
								file_bitacora.write(link + ' ' + obtener_hora() + '\n')
								links_encontrados.extend(temp_links)
								extractor.extraer_texto(html_crudo, link)
							except UnicodeEncodeError:	#En casos excepcionales la libreria de python encargada de visitar urls falla
								file_bitacora.write('Crawler no puede procesar caracter especial en ' + link + ' ' + obtener_hora() + '\n')
							except HTTPError:
								file_bitacora.write('Enlace roto o invalido: ' + link + ' ' + obtener_hora() + '\n')

						file_bitacora.write(semilla + ' completa ' + obtener_hora() + '\n')
					except UnicodeEncodeError:	#En casos excepcionales la libreria de python encargada de visitar urls falla
						file_bitacora.write('Crawler no puede procesar caracter especial en ' + semilla + ' ' + obtener_hora() + '\n')

				file_bitacora.write('Crawler finalizado ' + obtener_hora() + '\n')
			except KeyboardInterrupt:
				file_bitacora.write('Crawler finalizado ' + obtener_hora() + '\n')

class Extractor(object):
	def __init__(self, orden):
		self.indice = indice_invertido.ArbolB(orden, str)
		self.indice_inverso = indice_invertido.ArbolB(orden, str)
		self.stopwords = arbolb.ArbolB(4, str)
		with open(path[-1] + "\\cfg\\Stop_words.txt", "r") as stopwords:
			palabra = stopwords.readline()
			while palabra != '':
				self.stopwords.insertar(palabra)
				palabra = stopwords.readline()
		self.min_long = configuracion["INVERTED INDEX"]["min_long"]

	def extraer_texto(self, html_str, link):
		print('Extrayendo de', link)
		texto = dehtml(html_str)
		for palabra in re.findall(r'[a-zA-ZÀ-ÖØ-öø-ÿ]{' + self.min_long + ',}', texto):
			if self.stopwords.buscar(palabra):
				continue
			palabra = palabra.lower()
			palabra = unidecode(palabra)
			self.indice.insertar(palabra, link)
			self.indice_inverso.insertar(palabra[::-1], link)

	def devolver_indices(self):
		return (self.indice, self.indice_inverso)