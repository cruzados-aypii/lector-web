import re
import pickle
import os
from sys import path
path.append(os.path.abspath(os.path.dirname(__file__))[:-4])
from src.crawler import Crawler, Extractor
from indice_invertido import ClaveAjena


class Buscador(object):

	def preparar(self, orden):
		extractor = Extractor(orden)
		Crawler().crawl(extractor)
		self.indice, self.indice_inverso = extractor.devolver_indices()

	def consultar(self, palabra):
		return self.indice.obtener_dato(palabra)

	def consultar_inverso(self, sufijo):
		inverso = sufijo[::-1]
		limite_inferior = inverso[:-1] + chr(ord(inverso[-1])-1)
		limite_superior = inverso[:-1] + chr(ord(inverso[-1])+1)
		return self.indice_inverso.obtener_intervalo(limite_inferior, limite_superior)

def ejecutar():
	buscador = Buscador()
	print('Para interrumpir la recoleccion de datos presione CTRL+C')
	buscador.preparar(4)
	print('Bienvenido al buscador web de los Cruzados del Codigo. La frontera del buscador'+
		' esta definida en el archivo', path[-1]+'cfg\\cfg.')
	while True:
		print('\nSi desea cerrar la aplicacion, presione CTRL+C.\nEscriba la palabra que '+
		'desea buscar y recibira una lista con los links que contienen dicha palabra, o '+
		'escriba un * seguido de un sufijo para encontrar las palabras que terminan con '+
		'dicho sufijo. Para cargar la instancia de prueba del buscador, escriba "cargar_prueba".')
		try:
			comando = input()
		except KeyboardInterrupt:
			print('\nCerrando buscador...')
			exit()
		palabras = re.findall(r'\w+', comando)
		if comando == 'cargar_prueba':
			with open('indice_invertido.txt', 'rb') as file:
				buscador.indice = pickle.load(file)
			with open('indice_invertido_inverso.txt', 'rb') as file:
				buscador.indice_inverso = pickle.load(file)
			print('Instancia de prueba cargada.\n')
			continue

		if comando.startswith('*'):
			for w in palabras:
				w = w.lower()
				w = unidecode(w)
				matches = buscador.consultar_inverso(w)
				if len(matches) > 0:
					print('\nLas palabras que terminan con', w, 'se encuentran en los siguientes sitios web:')
					for link in matches:
						print(link)
				else:
					print('\nEl sufijo', w, 'no se encuentra en los sitios visitados.')
		else:
			for w in palabras:
				w = w.lower()
				w = unidecode(w)
				try:
					match = buscador.consultar(w)
					print('\nLa palabra', w, 'se encuentra en los siguientes sitios web:')
					for link in match:
						print(link)
				except ClaveAjena:
					print('\nLa palabra', w, 'no se encuentra en los sitios visitados.')

if __name__ == "__main__":
	ejecutar()