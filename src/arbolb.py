class NodoB(object):

	def __init__(self, es_hoja=False):
		self.es_hoja = es_hoja
		self.claves = []
		self.hijos = []

	"""Busca la clave indicada entre las claves del nodo.
	
	Argumentos:
	clave -- el elemento buscado
	
	Retorna una tupla: el primer miembro es un booleano, True si encuentra la
	clave, False de lo contrario; el segundo miembro es la posicion donde
	encontro o deberiar encontrar la clave.
	"""
	def consultar(self, clave):
		pos = 0
		while pos < len(self.claves):
			if self.claves[pos] == clave:
				if self.es_hoja:
					consulta = (True, pos)
				else:
					consulta = (True, pos+1)
				return consulta
			elif self.claves[pos] < clave:
				pos += 1
			else:
				break
		return (False, pos)

class ArbolB(object):

	"""Estructura de datos modelada en base a un arbol B.
	
	Argumentos:
	orden -- indica el orden del arbol. Los nodos de arbol de orden 5
	tendran a lo sumo 5 hijos, y cada nodo podra tener hasta 4 claves/datos.
	El orden no puede ser menor a 3. Si el orden es 3, el arbol funcionara,
	pero sera ineficiente con el manejo de la memoria.
	tipo -- tipo de los datos a almacenar en el arbol.
	
	Excepciones:
	OrdenAbsurdo -- levantada cuando orden < 3.
	"""
	def __init__(self, orden, tipo):
		if orden < 3:
			raise OrdenAbsurdo('El orden del arbol no puede ser menor a 3, de'+
			'otra forma esta implementacion no funciona.')
		self.raiz = NodoB(True)
		self.orden = orden
		self.tipo = tipo

	"""Busca la clave indicada en el arbol. Devuelve True si la encuentra, False
	de otra manera.
	"""
	def buscar(self, clave):
		if type(clave) != self.tipo:
			raise TipoIncorrecto('El tipo de la clave no coincide con el tipo de datos del arbol')

		nodo = self.raiz
		while True:
			encontrado, pos = nodo.consultar(clave)

			if encontrado:
				return True
			elif nodo.es_hoja:
				return False
			else:
				nodo = nodo.hijos[pos]

	"""Inserta la clave indicada en el arbol.
	
	Argumentos:
	clave -- el elemento a insertar en el arbol. Debe ser del mismo tipo
	que obtener_tipo()
	"""
	def insertar(self, clave):
		if type(clave) != self.tipo:
			raise TipoIncorrecto('El tipo de la clave no coincide con el tipo de datos del arbol')

		raiz = self.raiz
		#Si el nodo raiz esta lleno en el momento de insertar una clave,
		#este es partido de manera preventiva
		if len(raiz.claves) == (self.orden - 1):
			nraiz = NodoB()
			self.raiz = nraiz
			nraiz.hijos.append(raiz)
			
			self._partir_hijo(nraiz, 0)
			self._insertar(nraiz, clave)
		else:
			self._insertar(raiz, clave)

	"""Metodo interno de la clase, "parte" un nodo y aumenta la altura del arbol
	para mantener la condicion de arbol B.
	"""
	def _partir_hijo(self, padre, pos_hijo):
		hijo = padre.hijos[pos_hijo]
		nhijo = NodoB(hijo.es_hoja)
		orden = self.orden

		padre.hijos.insert(pos_hijo+1, nhijo)
		padre.claves.insert(pos_hijo, hijo.claves[int((orden-1)/2)])

		if nhijo.es_hoja:
			nhijo.claves = hijo.claves[int((orden-1)/2):]
		else:
			nhijo.claves = hijo.claves[int(((orden-1)/2)+1):]
		hijo.claves = hijo.claves[:int((orden-1)/2)]

		if not hijo.es_hoja:
			if self.orden%2 == 1:	#arbol de orden impar
				nhijo.hijos = hijo.hijos[int(orden/2+1):]
				hijo.hijos = hijo.hijos[:int(orden/2+1)]
			else:					#arbol de orden par
				nhijo.hijos = hijo.hijos[int(orden/2):]
				hijo.hijos = hijo.hijos[:int(orden/2)]


	"""Metodo interno de la clase, busca la hoja correspondiente del arbol
	e inserta la clave.
	"""
	def _insertar(self, nodo, clave):

		encontrado, pos = nodo.consultar(clave)

		if encontrado:
			return

		if nodo.es_hoja:
			nodo.claves.insert(pos, clave)
		else:
			#Si el nodo hijo esta lleno, se parte de manera preventiva,
			#antes de insertar el valor
			if len(nodo.hijos[pos].claves) == self.orden - 1:
				self._partir_hijo(nodo, pos)
				if clave >= nodo.claves[pos]:
					pos += 1
			self._insertar(nodo.hijos[pos], clave)

	"""Devuelve una lista ordenada con todos los datos almacenados de menor
	a mayor.
	"""
	def recorrer(self):
		claves = []
		self._recorrer(self.raiz, claves)
		return claves

	"""Metodo interno de la clase, busca las hojas del arbol y agrega sus datos
	a la lista a devolver.
	"""
	def _recorrer(self, nodo, claves):
		if nodo.es_hoja:
			for clave in nodo.claves:
				claves.append(clave)
		else:
			for hijo in nodo.hijos:
				self._recorrer(hijo, claves)

	"""Devuelve el tipo de los datos almacenados en el arbol.
	"""
	def obtener_tipo(self):
		return self.tipo

class OrdenAbsurdo(Exception):
	"""Excepcion levantada cuando se intenta construir un arbol B de orden menor
	a 3
	"""
	pass
class TipoIncorrecto(Exception):
	"""Excepcion levantada cuando se intenta insertar o buscar un elemento de
	tipo distinto al de los datos almacenados en el arbol"""
	pass