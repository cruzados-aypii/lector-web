#Esta implementacion de arbol B fue adaptada para guardar listas con claves strings
class NodoB(object):

	def __init__(self, es_hoja=False):
		self.es_hoja = es_hoja
		self.claves = []
		self.hijos = []

	"""Busca la clave indicada entre las claves del nodo.
	
	Argumentos:
	clave -- el elemento buscado
	
	Retorna una tupla: el primer miembro es un booleano, True si encuentra la
	clave, False de lo contrario; el segundo miembro es la posicion donde
	encontro o deberia encontrar la clave.
	"""
	def consultar(self, clave):
		pos = 0
		if self.es_hoja:
			while pos < len(self.claves):
				if self.claves[pos][0] == clave:
					return (True, pos)
				elif self.claves[pos][0] < clave:
					pos += 1
				else:
					break
		else:
			while pos < len(self.claves):
				if self.claves[pos] == clave:
					return (True, pos+1)
				elif self.claves[pos] < clave:
					pos += 1
				else:
					break
		return (False, pos)

class ArbolB(object):

	"""Estructura de datos modelada en base a un arbol B.
	
	Argumentos:
	orden -- indica el orden del arbol. Los nodos de arbol de orden 5
	tendran a lo sumo 5 hijos, y cada nodo podra tener hasta 4 claves/datos.
	El orden no puede ser menor a 3. Si el orden es 3, el arbol funcionara,
	pero sera ineficiente con el manejo de la memoria.
	tipo -- tipo de los datos a almacenar en el arbol.
	
	Excepciones:
	OrdenAbsurdo -- levantada cuando orden < 3.
	"""
	def __init__(self, orden, tipo):
		if orden < 3:
			raise OrdenAbsurdo('El orden del arbol no puede ser menor a 3, de'+
			'otra forma esta implementacion no funciona.')
		self.raiz = NodoB(True)
		self.orden = orden
		self.tipo = tipo

	"""Busca la clave indicada en el arbol. Devuelve True si la encuentra, False
	de otra manera.
	"""
	def buscar(self, clave):
		nodo = self.raiz
		while True:
			encontrado, pos = nodo.consultar(clave)

			if encontrado:
				return True
			elif nodo.es_hoja:
				return False
			else:
				nodo = nodo.hijos[pos]

	"""Busca el dato/s almacenado segun la clave indicada.
	
	Argumentos:
	clave -- la clave asignada al dato/s guardado. buscar(clave) deberia
	devolver True.
	
	Excepciones:
	ClaveAjena -- levantada cuando la clave indicada no pertenece al arbol.
	"""
	def obtener_dato(self, clave):
		nodo = self.raiz
		while True:
			encontrado, pos = nodo.consultar(clave)

			if encontrado and nodo.es_hoja:
				return nodo.claves[pos][1]
			elif nodo.es_hoja:
				raise ClaveAjena('La clave indicada no pertenece al arbol')
			else:
				nodo = nodo.hijos[pos]

	"""Busca todas las claves dentro del intervalo definido por limite_inferior
	y limite_superior, extremos excluidos, y devuelve los datos correspondientes
	con esas claves.
	
	Argumentos:
	limite_inferior -- cota inferior del intervalo.
	limite_superior -- cota superior del intervalo.
	"""
	def obtener_intervalo(self, limite_inferior, limite_superior):
		datos = []
		limites = (limite_inferior, limite_superior)
		self._obtener_intervalo(datos, limites, self.raiz)
		lista_limpia = []

		for dato in datos:	#remueve datos repetidos
			if dato not in lista_limpia:
				lista_limpia.append(dato)
		return lista_limpia

	"""Metodo interno de la clase, busca las hojas y agrega a la lista
	los datos cuyas claves se encuentran en el intervalo definido por
	limites.
	"""
	def _obtener_intervalo(self, datos, limites, nodo):
		pos = 0
#el ciclo se incluye dentro del if/else para no revisar la condicion en cada iteracion
		if nodo.es_hoja:
			while pos < len(nodo.claves):
#si es mayor al limite superior, entonces no hay mas claves a la derecha de este elemento que entren
#en el intervalo
				if nodo.claves[pos][0] < limites[1]:
#si es menor que el limite inferior aun se puede encontrar claves a la derecha de este elementos que
#entre en el intervalo
					if nodo.claves[pos][0] > limites[0]:
						datos.extend(nodo.claves[pos][1])
				else:
					break
				pos += 1
		else:
			#Se desplaza por las claves de izquierda a derecha
			while pos < len(nodo.claves):

				if nodo.claves[pos] < limites[1] and nodo.claves[pos] > limites[0]:
					#Si la clave esta entre los limites, cualquiera de sus hijos puede contener
					#datos utiles, pero los hijos a la derecha seran visitados al avanzar con el
					#ciclo, a menos que la clave se la ultima en el nodo
					self._obtener_intervalo(datos, limites, nodo.hijos[pos])
					if pos == (len(nodo.claves) - 1):
						self._obtener_intervalo(datos, limites, nodo.hijos[pos+1])

				elif nodo.claves[pos] <= limites[0]:
					#Si la clave es menor o igual al limite inferior, los datos utiles solo
					#podrian estar a la derecha de esta clave, pero los hijos a la derecha seran
					#visitados al avanzar con el ciclo, a menos que la clave sea la ultima en
					#el nodo
					if pos == (len(nodo.claves) - 1):
						self._obtener_intervalo(datos, limites, nodo.hijos[pos+1])

				else:
					#Si la clave es mayor o igual al limite superior, los datos utiles solo
					#podrian estar a la izquierda de esta clave, por lo que luego de visitar
					#el hijo izquiero el ciclo cierra
					self._obtener_intervalo(datos, limites, nodo.hijos[pos])
					break
				pos += 1

	"""Inserta el dato con la clave indicada en el arbol.
	
	Argumentos:
	clave -- la clave del dato a insertar en el arbol.
	dato -- el dato a insertar en el arbol. Debe ser del mismo tipo
	que obtener_tipo().
	"""
	def insertar(self, clave, dato):
		if type(dato) != self.tipo:
			raise TipoIncorrecto('El tipo de la clave no coincide con el tipo de datos del arbol')

		raiz = self.raiz
		#Si el nodo raiz esta lleno en el momento de insertar una clave,
		#este es partido de manera preventiva
		if len(raiz.claves) == (self.orden - 1):
			nraiz = NodoB()
			self.raiz = nraiz
			nraiz.hijos.append(raiz)
			
			self._partir_hijo(nraiz, 0)
			self._insertar(nraiz, clave, dato)
		else:
			self._insertar(raiz, clave, dato)

	"""Metodo interno de la clase, "parte" un nodo y aumenta la altura del arbol
	para mantener la condicion de arbol B.
	"""
	def _partir_hijo(self, padre, pos_hijo):
		hijo = padre.hijos[pos_hijo]
		nhijo = NodoB(hijo.es_hoja)
		orden = self.orden

		padre.hijos.insert(pos_hijo+1, nhijo)
		if hijo.es_hoja:
			padre.claves.insert(pos_hijo, hijo.claves[int((orden-1)/2)][0])
		else:
			padre.claves.insert(pos_hijo, hijo.claves[int((orden-1)/2)])

		if nhijo.es_hoja:
			nhijo.claves = hijo.claves[int((orden-1)/2):]
		else:
			nhijo.claves = hijo.claves[int(((orden-1)/2)+1):]
		hijo.claves = hijo.claves[:int((orden-1)/2)]

		if not hijo.es_hoja:
			if self.orden%2 == 1:	#arbol de orden impar
				nhijo.hijos = hijo.hijos[int(orden/2+1):]
				hijo.hijos = hijo.hijos[:int(orden/2+1)]
			else:					#arbol de orden par
				nhijo.hijos = hijo.hijos[int(orden/2):]
				hijo.hijos = hijo.hijos[:int(orden/2)]


	"""Metodo interno de la clase, busca la hoja correspondiente del arbol
	e inserta el dato con su clave.
	"""
	def _insertar(self, nodo, clave, dato):

		encontrado, pos = nodo.consultar(clave)

		if nodo.es_hoja:
			if encontrado:
				if self._no_repetido(dato, nodo.claves[pos][1]):
					nodo.claves[pos][1].append(dato)
			else:
				nodo.claves.insert(pos, (clave, [dato]))
		else:
			#Si el nodo hijo esta lleno, se parte de manera preventiva,
			#antes de insertar el valor
			if len(nodo.hijos[pos].claves) == self.orden - 1:
				self._partir_hijo(nodo, pos)
				if clave >= nodo.claves[pos]:
					pos += 1
			self._insertar(nodo.hijos[pos], clave, dato)

	"""Devuelve una lista ordenada con todos los datos almacenados de menor
	a mayor segun sus claves.
	"""
	def recorrer(self):
		claves = []
		self._recorrer(self.raiz, claves)
		return claves

	"""Metodo interno de la clase, busca las hojas del arbol y agrega sus datos
	a la lista a devolver.
	"""
	def _recorrer(self, nodo, claves):
		if nodo.es_hoja:
			for clave in nodo.claves:
				claves.append(clave)
		else:
			for hijo in nodo.hijos:
				self._recorrer(hijo, claves)

	"""Metodo interno de la clase, consulta la lista de datos correspondiente
	a una clave del arbol para comprobar si un dato ya fue almacenado.
	"""
	def _no_repetido(self, dato, lista_datos):
		for elemento in lista_datos:
			if dato == elemento:
				return False
		return True

	"""Devuelve el tipo de los datos almacenados en el arbol.
	"""
	def obtener_tipo(self):
		return self.tipo

class OrdenAbsurdo(Exception):
	"""Excepcion levantada cuando se intenta construir un arbol B de orden menor
	a 3.
	"""
	pass

class TipoIncorrecto(Exception):
	"""Excepcion levantada cuando se intenta insertar o buscar un elemento de
	tipo distinto al de los datos almacenados en el arbol.
	"""
	pass

class ClaveAjena(Exception):
	"""Excepcion levantada cuando se intenta obtener un dato con una clave que
	no pertenece al arbol.
	"""
	pass