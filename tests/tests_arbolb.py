import unittest
import os
from sys import path
path.append(os.path.abspath(os.path.dirname(__file__))[:-5])
from src.arbolb import *

class TestArbolB(unittest.TestCase):
	def setUp(self):
		self.arbol = ArbolB(4, int)

	def test_nodo_individual(self):
		nodo = NodoB()
		self.assertEqual(len(self.arbol.recorrer()), 0)
		self.assertFalse(nodo.es_hoja)
		self.assertEqual(len(nodo.claves), 0)
		self.assertEqual(len(nodo.hijos), 0)

	def test_raiz(self):
		claves = [1, 3, 7]
		self.assertEqual(len(self.arbol.recorrer()), 0)
		self.assertFalse(self.arbol.buscar(1))
		self.arbol.insertar(1)
		self.assertEqual(self.arbol.raiz.claves[0], 1)
		self.assertTrue(self.arbol.buscar(1))
		self.arbol.insertar(7)
		self.arbol.insertar(3)
		self.assertEqual(self.arbol.recorrer(), claves)
		self.assertEqual(self.arbol.raiz.claves, claves)
		self.assertTrue(self.arbol.buscar(3))
		self.assertTrue(self.arbol.buscar(7))

	def test_arbol_grande_orden_par(self):
		claves = [1, 2, 3, 5, 7, 9, 10, 11, 15, 18, 21, 26, 27, 29]
		self.arbol.insertar(1)
		self.arbol.insertar(3)
		self.arbol.insertar(15)
		self.arbol.insertar(7)
		self.arbol.insertar(5)
		self.arbol.insertar(29)
		self.arbol.insertar(21)
		self.arbol.insertar(11)
		self.arbol.insertar(10)
		self.arbol.insertar(2)
		self.arbol.insertar(26)
		self.arbol.insertar(27)
		self.arbol.insertar(18)
		self.arbol.insertar(9)
		self.assertEqual(self.arbol.raiz.claves[0], 7)
		self.assertEqual(self.arbol.raiz.hijos[0].claves[0], 3)
		self.assertEqual(self.arbol.raiz.hijos[0].hijos[0].claves[0], 1)
		self.assertEqual(self.arbol.raiz.hijos[0].hijos[0].claves[1], 2)
		self.assertEqual(self.arbol.raiz.hijos[0].hijos[1].claves[0], 3)
		self.assertEqual(self.arbol.raiz.hijos[0].hijos[1].claves[1], 5)
		self.assertEqual(self.arbol.raiz.hijos[1].claves[0], 10)
		self.assertEqual(self.arbol.raiz.hijos[1].claves[1], 15)
		self.assertEqual(self.arbol.raiz.hijos[1].hijos[0].claves[0], 7)
		self.assertEqual(self.arbol.raiz.hijos[1].hijos[0].claves[1], 9)
		self.assertEqual(self.arbol.raiz.hijos[1].hijos[1].claves[0], 10)
		self.assertEqual(self.arbol.raiz.hijos[1].hijos[1].claves[1], 11)
		self.assertEqual(self.arbol.raiz.hijos[1].hijos[2].claves[0], 15)
		self.assertEqual(self.arbol.raiz.hijos[1].hijos[2].claves[1], 18)
		self.assertEqual(self.arbol.raiz.claves[1], 21)
		self.assertEqual(self.arbol.raiz.hijos[2].claves[0], 26)
		self.assertEqual(self.arbol.raiz.hijos[2].hijos[0].claves[0], 21)
		self.assertEqual(self.arbol.raiz.hijos[2].hijos[1].claves[0], 26)
		self.assertEqual(self.arbol.raiz.hijos[2].hijos[1].claves[1], 27)
		self.assertEqual(self.arbol.raiz.hijos[2].hijos[1].claves[2], 29)
		self.assertEqual(self.arbol.recorrer(), claves)

	def test_arbol_grande_orden_impar(self):
		claves = [1, 2, 3, 5, 7, 9, 10, 11, 15, 18, 21, 26, 27, 29]
		arbol = ArbolB(5, int)
		arbol.insertar(1)
		arbol.insertar(3)
		arbol.insertar(15)
		arbol.insertar(7)
		arbol.insertar(5)
		arbol.insertar(29)
		arbol.insertar(21)
		arbol.insertar(11)
		arbol.insertar(10)
		arbol.insertar(2)
		arbol.insertar(26)
		arbol.insertar(27)
		arbol.insertar(18)
		arbol.insertar(9)
		self.assertEqual(arbol.raiz.claves[0], 7)
		self.assertEqual(arbol.raiz.hijos[0].claves[0], 1)
		self.assertEqual(arbol.raiz.hijos[0].claves[1], 2)
		self.assertEqual(arbol.raiz.hijos[0].claves[2], 3)
		self.assertEqual(arbol.raiz.hijos[0].claves[3], 5)
		self.assertEqual(arbol.raiz.claves[1], 11)
		self.assertEqual(arbol.raiz.hijos[1].claves[0], 7)
		self.assertEqual(arbol.raiz.hijos[1].claves[1], 9)
		self.assertEqual(arbol.raiz.hijos[1].claves[2], 10)
		self.assertEqual(arbol.raiz.claves[2], 21)
		self.assertEqual(arbol.raiz.hijos[2].claves[0], 11)
		self.assertEqual(arbol.raiz.hijos[2].claves[1], 15)
		self.assertEqual(arbol.raiz.hijos[2].claves[2], 18)
		self.assertEqual(arbol.raiz.hijos[3].claves[0], 21)
		self.assertEqual(arbol.raiz.hijos[3].claves[1], 26)
		self.assertEqual(arbol.raiz.hijos[3].claves[2], 27)
		self.assertEqual(arbol.raiz.hijos[3].claves[3], 29)
		self.assertEqual(arbol.recorrer(), claves)

	def test_arbol_masivo(self):
		for x in range(10000):
			self.arbol.insertar(x)
		self.assertEqual(self.arbol.recorrer(), list(range(10000)))
		
		for x in range(10000):
			self.arbol.insertar(x)	#no inserta valores repetidos
		self.assertEqual(self.arbol.recorrer(), list(range(10000)))

		arbol_impar = ArbolB(5, int)
		for x in range(10000):
			arbol_impar.insertar(x)
		self.assertEqual(arbol_impar.recorrer(), list(range(10000)))

		for x in range(10000):
			arbol_impar.insertar(x)	#no inserta valores repetidos
		self.assertEqual(arbol_impar.recorrer(), list(range(10000)))

	def test_excepciones(self):
		self.assertRaises(OrdenAbsurdo, lambda: ArbolB(2, str))
		self.assertRaises(OrdenAbsurdo, lambda: ArbolB(-2, str))
		self.assertRaises(OrdenAbsurdo, lambda: ArbolB(-5, str))

		self.assertRaises(TipoIncorrecto, lambda: self.arbol.insertar('avc'))

if __name__ == "__main__":
    unittest.main()