import unittest
import os
from sys import path
path.append(os.path.abspath(os.path.dirname(__file__))[:-5])
from src.indice_invertido import *

class Testarbol_parB(unittest.TestCase):
	def setUp(self):
		self.arbol_par = ArbolB(4, str)
		self.arbol_impar = ArbolB(5, str)

	def test_nodo_individual(self):
		nodo = NodoB()
		self.assertEqual(len(self.arbol_par.recorrer()), 0)
		self.assertFalse(nodo.es_hoja)
		self.assertEqual(len(nodo.claves), 0)
		self.assertEqual(len(nodo.hijos), 0)

	def test_raiz(self):
		claves = [(1, ['dato1']), (3, ['dato3']), (7, ['dato2'])]

		self.assertEqual(len(self.arbol_par.recorrer()), 0)
		self.assertFalse(self.arbol_par.buscar(1))
		self.arbol_par.insertar(1, 'dato1')
		self.assertEqual(self.arbol_par.raiz.claves[0], claves[0])
		self.assertTrue(self.arbol_par.buscar(1))
		self.arbol_par.insertar(7, 'dato2')
		self.arbol_par.insertar(3, 'dato3')

		self.assertEqual(self.arbol_par.recorrer(), claves)
		self.assertEqual(self.arbol_par.raiz.claves, claves)
		for tupla in self.arbol_par.raiz.claves:
			self.assertTrue(self.arbol_par._no_repetido('dato4', tupla[1]))
		self.assertFalse(self.arbol_par._no_repetido('dato1', self.arbol_par.raiz.claves[0][1]))
		self.assertFalse(self.arbol_par._no_repetido('dato3', self.arbol_par.raiz.claves[1][1]))
		self.assertFalse(self.arbol_par._no_repetido('dato2', self.arbol_par.raiz.claves[2][1]))
		self.assertTrue(self.arbol_par.buscar(3))
		self.assertTrue(self.arbol_par.buscar(7))

	def test_arbol_grande_orden_par(self):
		claves = [(1, ['dato1']), (2, ['dato10']), (3, ['dato2']), (5, ['dato5']), (7, ['dato4']),\
		(9, ['dato14']), (10, ['dato9']), (11, ['dato8']), (15, ['dato3']), (18, ['dato13']),\
		(21, ['dato7']), (26, ['dato11']), (27, ['dato12']), (29, ['dato6'])]
		self.arbol_par.insertar(1, 'dato1')
		self.arbol_par.insertar(3, 'dato2')
		self.arbol_par.insertar(15, 'dato3')
		self.arbol_par.insertar(7, 'dato4')
		self.arbol_par.insertar(5, 'dato5')
		self.arbol_par.insertar(29, 'dato6')
		self.arbol_par.insertar(21, 'dato7')
		self.arbol_par.insertar(11, 'dato8')
		self.arbol_par.insertar(10, 'dato9')
		self.arbol_par.insertar(2, 'dato10')
		self.arbol_par.insertar(26, 'dato11')
		self.arbol_par.insertar(27, 'dato12')
		self.arbol_par.insertar(18, 'dato13')
		self.arbol_par.insertar(9, 'dato14')
		self.assertEqual(self.arbol_par.raiz.claves[0], 7)
		self.assertEqual(self.arbol_par.raiz.hijos[0].claves[0], 3)
		self.assertEqual(self.arbol_par.raiz.hijos[0].hijos[0].claves[0], claves[0])
		self.assertEqual(self.arbol_par.raiz.hijos[0].hijos[0].claves[1], claves[1])
		self.assertEqual(self.arbol_par.raiz.hijos[0].hijos[1].claves[0], claves[2])
		self.assertEqual(self.arbol_par.raiz.hijos[0].hijos[1].claves[1], claves[3])
		self.assertEqual(self.arbol_par.raiz.hijos[1].claves[0], 10)
		self.assertEqual(self.arbol_par.raiz.hijos[1].claves[1], 15)
		self.assertEqual(self.arbol_par.raiz.hijos[1].hijos[0].claves[0], claves[4])
		self.assertEqual(self.arbol_par.raiz.hijos[1].hijos[0].claves[1], claves[5])
		self.assertEqual(self.arbol_par.raiz.hijos[1].hijos[1].claves[0], claves[6])
		self.assertEqual(self.arbol_par.raiz.hijos[1].hijos[1].claves[1], claves[7])
		self.assertEqual(self.arbol_par.raiz.hijos[1].hijos[2].claves[0], claves[8])
		self.assertEqual(self.arbol_par.raiz.hijos[1].hijos[2].claves[1], claves[9])
		self.assertEqual(self.arbol_par.raiz.claves[1], 21)
		self.assertEqual(self.arbol_par.raiz.hijos[2].claves[0], 26)
		self.assertEqual(self.arbol_par.raiz.hijos[2].hijos[0].claves[0], claves[10])
		self.assertEqual(self.arbol_par.raiz.hijos[2].hijos[1].claves[0], claves[11])
		self.assertEqual(self.arbol_par.raiz.hijos[2].hijos[1].claves[1], claves[12])
		self.assertEqual(self.arbol_par.raiz.hijos[2].hijos[1].claves[2], claves[13])
		self.assertEqual(self.arbol_par.recorrer(), claves)

	def test_arbol_grande_orden_impar(self):
		claves = [(1, ['dato1']), (2, ['dato10']), (3, ['dato2']), (5, ['dato5']), (7, ['dato4']),\
		(9, ['dato14']), (10, ['dato9']), (11, ['dato8']), (15, ['dato3']), (18, ['dato13']),\
		(21, ['dato7']), (26, ['dato11']), (27, ['dato12']), (29, ['dato6'])]
		self.arbol_impar.insertar(1, 'dato1')
		self.arbol_impar.insertar(3, 'dato2')
		self.arbol_impar.insertar(15, 'dato3')
		self.arbol_impar.insertar(7, 'dato4')
		self.arbol_impar.insertar(5, 'dato5')
		self.arbol_impar.insertar(29, 'dato6')
		self.arbol_impar.insertar(21, 'dato7')
		self.arbol_impar.insertar(11, 'dato8')
		self.arbol_impar.insertar(10, 'dato9')
		self.arbol_impar.insertar(2, 'dato10')
		self.arbol_impar.insertar(26, 'dato11')
		self.arbol_impar.insertar(27, 'dato12')
		self.arbol_impar.insertar(18, 'dato13')
		self.arbol_impar.insertar(9, 'dato14')
		self.assertEqual(self.arbol_impar.raiz.claves[0], 7)
		self.assertEqual(self.arbol_impar.raiz.hijos[0].claves[0], claves[0])
		self.assertEqual(self.arbol_impar.raiz.hijos[0].claves[1], claves[1])
		self.assertEqual(self.arbol_impar.raiz.hijos[0].claves[2], claves[2])
		self.assertEqual(self.arbol_impar.raiz.hijos[0].claves[3], claves[3])
		self.assertEqual(self.arbol_impar.raiz.claves[1], 11)
		self.assertEqual(self.arbol_impar.raiz.hijos[1].claves[0], claves[4])
		self.assertEqual(self.arbol_impar.raiz.hijos[1].claves[1], claves[5])
		self.assertEqual(self.arbol_impar.raiz.hijos[1].claves[2], claves[6])
		self.assertEqual(self.arbol_impar.raiz.claves[2], 21)
		self.assertEqual(self.arbol_impar.raiz.hijos[2].claves[0], claves[7])
		self.assertEqual(self.arbol_impar.raiz.hijos[2].claves[1], claves[8])
		self.assertEqual(self.arbol_impar.raiz.hijos[2].claves[2], claves[9])
		self.assertEqual(self.arbol_impar.raiz.hijos[3].claves[0], claves[10])
		self.assertEqual(self.arbol_impar.raiz.hijos[3].claves[1], claves[11])
		self.assertEqual(self.arbol_impar.raiz.hijos[3].claves[2], claves[12])
		self.assertEqual(self.arbol_impar.raiz.hijos[3].claves[3], claves[13])
		self.assertEqual(self.arbol_impar.recorrer(), claves)

	def test_arbol_masivo(self):
		datos = []
		for x in range(10000):
			datos.append((x+1, ['dato' + str(x+1)]))

		for dato in datos:
			self.arbol_par.insertar(dato[0], dato[1][0])
		self.assertEqual(self.arbol_par.recorrer(), datos)

		for dato in datos:
			self.arbol_par.insertar(dato[0], dato[1][0])	#no inserta valores repetidos
		self.assertEqual(self.arbol_par.recorrer(), datos)

		for dato in datos:
			self.arbol_impar.insertar(dato[0], dato[1][0])
		self.assertEqual(self.arbol_impar.recorrer(), datos)

		for dato in datos:
			self.arbol_impar.insertar(dato[0], dato[1][0])	#no inserta valores repetidos
		self.assertEqual(self.arbol_impar.recorrer(), datos)

	def test_insertar_datos_con_una_misma_clave(self):
		claves = [(1, ['dato1', 'dato4', 'dato5']), (3, ['dato8']), (6, ['dato3', 'dato7']), (11, ['dato2', 'dato6'])]
		self.arbol_par.insertar(1, 'dato1')
		self.arbol_par.insertar(11, 'dato2')
		self.arbol_par.insertar(6, 'dato3')
		self.arbol_par.insertar(1, 'dato4')
		self.arbol_par.insertar(1, 'dato5')
		self.arbol_par.insertar(11, 'dato6')
		self.arbol_par.insertar(6, 'dato7')
		self.arbol_par.insertar(3, 'dato8')
		self.assertEqual(self.arbol_par.recorrer(), claves)

		self.arbol_impar.insertar(1, 'dato1')
		self.arbol_impar.insertar(11, 'dato2')
		self.arbol_impar.insertar(6, 'dato3')
		self.arbol_impar.insertar(1, 'dato4')
		self.arbol_impar.insertar(1, 'dato5')
		self.arbol_impar.insertar(11, 'dato6')
		self.arbol_impar.insertar(6, 'dato7')
		self.arbol_impar.insertar(3, 'dato8')
		self.assertEqual(self.arbol_impar.recorrer(), claves)

	def obtener_intervalo(self):
		intervalo = ['dato1', 'dato4', 'dato5', 'dato9']
		self.arbol_par.insertar(3, 'dato1')
		self.arbol_par.insertar(2, 'dato2')
		self.arbol_par.insertar(27, 'dato3')
		self.arbol_par.insertar(15, 'dato4')
		self.arbol_par.insertar(15, 'dato5')
		self.arbol_par.insertar(21, 'dato6')
		self.arbol_par.insertar(17, 'dato1')
		self.arbol_par.insertar(1034, 'dato8')
		self.arbol_par.insertar(10, 'dato9')

		datos = self.arbol_par.obtener_intervalo(2, 21)
		for dato in datos:
			assertTrue(dato in intervalo)	#los datos encontrado entran en el intervalo
		for dato in intervalo:
			assertEqual(datos.count(dato), 1)	#los datos encontrados no se repiten

	def test_excepciones(self):
		self.assertRaises(OrdenAbsurdo, lambda: ArbolB(2, str))
		self.assertRaises(OrdenAbsurdo, lambda: ArbolB(-2, str))
		self.assertRaises(OrdenAbsurdo, lambda: ArbolB(-5, str))

		for x in range(20):
			self.arbol_impar.insertar(x+1, 'dato' + str(x+1))
		self.assertEqual(self.arbol_impar.obtener_dato(10), ['dato10'])
		self.assertRaises(ClaveAjena, lambda: self.arbol_impar.obtener_dato(21))
		self.assertRaises(ClaveAjena, lambda: self.arbol_impar.obtener_dato(0))
		self.assertRaises(ClaveAjena, lambda: self.arbol_impar.obtener_dato(-10))
		self.assertRaises(ClaveAjena, lambda: self.arbol_impar.obtener_dato(-21))

		self.assertRaises(TipoIncorrecto, lambda: self.arbol_impar.insertar(10, 10))

if __name__ == "__main__":
	unittest.main()